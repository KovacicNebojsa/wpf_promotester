﻿using System;
using System.Windows;

namespace PromoTester1
{
    /// <summary>
    /// Interaction logic for WebCamSelectDialog.xaml
    /// </summary>
    public partial class WebCamSelectDialog : Window
    {
        private int selectedWebcamId;
        public int SelectedWebcamId
        {
            get { return selectedWebcamId; }
        }
        public WebCamSelectDialog()
        {
            InitializeComponent();
        }

        public WebCamSelectDialog(String[] webCams)
        {
            InitializeComponent();
            selectedWebcamId = -1;
            for (int i = 0; i < webCams.Length; i++)
            {
                comboBoxCamSelect.Items.Add(webCams[i]);
            }

            if (comboBoxCamSelect.Items.Count > 0)
            {
                if (PromoTester1.Properties.Settings.Default.webCamId > 0)
                {
                    comboBoxCamSelect.SelectedIndex = PromoTester1.Properties.Settings.Default.webCamId;
                }
                else
                {
                    comboBoxCamSelect.SelectedIndex = 0;
                }
            }
        }


        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            selectedWebcamId = comboBoxCamSelect.SelectedIndex;
            this.DialogResult = true;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
