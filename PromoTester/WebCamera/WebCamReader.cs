﻿using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows;

namespace PromoTester1
{
    public class WebCamReader
    {
        #region Fields
        private bool isDummyWebCam;
        private int webCamId;

        private IntPtr currentImagePtr;
        public IntPtr CurrentImagePtr
        {
            get { lock (this) { return currentImagePtr; } }
        }

        private bool webCamStarted;
        public bool WebCamStarted
        {
            get { lock (this) { return webCamStarted; } }
        }

        private bool recStarted;
        public bool RecStarted
        {
            get { lock (this) { return recStarted; } }
        }

        private bool camCheckStarted;// indicates if camera is connected! If not, camOk is set to false!
        public bool CamCheckStarted
        {
            get { lock (this) { return camCheckStarted; } }
        }

        private bool camOk;// indicates if camera is connected! If not, camOk is set to false!
        public bool CamOk
        {
            get { lock (this) { return camOk; } }
        }

        public int TestedRecFps { get; set; }

        private int zoom;
        #endregion

        #region Constructors

        /*!
        * Dummy constructor, used for NO WEBCAM app mode.
        */
        public WebCamReader()
        {
            isDummyWebCam = true;
            camOk = true;
        }


        /*!
         * Main constructor, creates capture and initializes web camera.
         */
        public WebCamReader(int camId)
        {
            isDummyWebCam = false;

            currentImagePtr = IntPtr.Zero;
            recStarted = false;
            zoom = 0;

            String video_input_list;
            StringBuilder tmp_video_input_list = new StringBuilder(1000);

            if (camId >= 0)
            {
                webCamId = camId;
            }
            else
            {
                webCamId = OpenCVCamera.FindVideoInputandList(tmp_video_input_list);
            }

            if (webCamId < 0)
            {
                video_input_list = tmp_video_input_list.ToString();
                String[] webcams = video_input_list.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                if (webcams.Length > 0)
                {
                    WebCamSelectDialog wcsd = new WebCamSelectDialog(webcams);
                    bool? result = wcsd.ShowDialog();
                    if (result == true)
                    {
                        webCamId = wcsd.SelectedWebcamId;
                    }
                    else
                    {
                        throw new WebCamReaderException(PromoTester1.Properties.Resources.noWebcamError, WebCamReaderException.ERROR_MESSAGE);
                    }
                }
                else
                {
                    throw new WebCamReaderException(PromoTester1.Properties.Resources.noWebcamError, WebCamReaderException.ERROR_MESSAGE);
                }
            }

            // Starts webcam
            //rec_image_width,rec_image_height
            int errCode = OpenCVCamera.createCaptureDevice(webCamId, 640, 480);
            if (errCode == 0)
            {
                lock (this)
                {
                    webCamStarted = true;
                    camCheckStarted = false;
                    camOk = true;


                    //min_rec_video_fps
                    TestedRecFps = 5;
                    //TestedRecFps = 20;
                    //testRecordSpeed(out testedRecFps);
                }
            }
            else if (errCode == 1)
            {
                OpenCVCamera.disposeCapture();
                webCamStarted = false;
                throw new WebCamReaderException(PromoTester1.Properties.Resources.webcamReader_resolutionWarning, WebCamReaderException.WARNING_MESSAGE);
            }
            else
            { 
                throw new WebCamReaderException(PromoTester1.Properties.Resources.cameraDisconnectedError, WebCamReaderException.ERROR_MESSAGE);
            }
        }

        #endregion

        public static byte[] BitmapToBytes(Bitmap bmp)
        {
            if (bmp != null)
            {
                MemoryStream imgStream = new MemoryStream();
                bmp.Save(imgStream, System.Drawing.Imaging.ImageFormat.Bmp);

                return imgStream.ToArray();
            }
            else
            {
                return new byte[0];
            }
        }

        public void zoomIn()
        {
            if (zoom < 4)
                zoom++;

        }
        public void zoomOut()
        {
            if (zoom > 0)
                zoom--;
        }

        /*!
         * Detects maximum recording fps rate and sets rec_video_fps to curent value.
         */
        public void testRecordSpeed(out int recVideoFps)
        {
            int start = DateTime.Now.Hour * 3600000 + DateTime.Now.Minute * 60000 + DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;
            for (int i = 0; i < 15; i++)
            {
                nextImage();
            }
            int stop = DateTime.Now.Hour * 3600000 + DateTime.Now.Minute * 60000 + DateTime.Now.Second * 1000 + DateTime.Now.Millisecond;
            recVideoFps = (int)(15000 / (stop - start));

            if (recVideoFps < PromoTester1.Properties.Settings.Default.min_rec_video_fps)
            {
                camOk = false;
                throw new WebCamReaderException(PromoTester1.Properties.Resources.webcamReader_fpsWarning, WebCamReaderException.WARNING_MESSAGE);
            }
        }

        /*!
         * Gets next frame from webCam capture and loads it in currentImage field.
         */
        public void nextImage()
        {
            if (!isDummyWebCam)
            {
                if (CamOk)
                {
                    int end = 0;

                    lock (this)
                    {
                        IntPtr ptr = OpenCVCamera.queryFrame(ref end, zoom);
                        currentImagePtr = (end == 0) ? ptr : IntPtr.Zero;
                    }
                }
                else
                {
                    throw new WebCamReaderException(PromoTester1.Properties.Resources.cameraDisconnectedError, WebCamReaderException.ERROR_MESSAGE);
                }
            }
        }

        public void writeToAvi()
        {
            if (!isDummyWebCam)
            {
                int w = PromoTester1.Properties.Settings.Default.rec_image_width;
                int h = PromoTester1.Properties.Settings.Default.rec_image_height;

                lock (this)
                {
                    if (recStarted)
                    {
                        if (OpenCVCamera.writeFrame() != 0)
                        {
                            throw new Exception(PromoTester1.Properties.Resources.recordingError);
                        }
                        else
                        {
                            // DO NOTHING
                        }
                    }
                }
            }
        }

        /*!
         *  Gets currentImage converted in Bitmap.
         *  
         * 
         *  \return currentImage in Bitmap format
         */
        public Bitmap getWebCamShot()
        {
            if (isDummyWebCam)
            {
                int w = PromoTester1.Properties.Settings.Default.rec_image_width;
                int h = PromoTester1.Properties.Settings.Default.rec_image_height;
                return new Bitmap(w, h);
            }
            else
            {
                Bitmap bmp;
                int w = PromoTester1.Properties.Settings.Default.rec_image_width;
                int h = PromoTester1.Properties.Settings.Default.rec_image_height;

                lock (this)
                {
                    bmp = new Bitmap(w, h, w * 3, System.Drawing.Imaging.PixelFormat.Format24bppRgb, currentImagePtr);
                }
                return bmp;
            }
        }

        /*!
         *  Creates new video file path if filePath already exists
         *  
         * \param [in] String filePath - file path which have to be checked and eventualy changed
         * \return New filePath, with added sufix 1,2,3... on its name.
         */
        private String getFilePath(String filePath)
        {
            String rez = filePath;

            FileInfo fi = new FileInfo(filePath);
            int index = filePath.LastIndexOf(".");
            int sufix = 1;

            // if filePath exists, adds sufix 1 at the end of its name. If that name also exists, repeat procedure by increasing sufix.  
            while (fi.Exists)
            {
                rez = filePath.Insert(index, sufix.ToString());
                fi = new FileInfo(rez);
                sufix++;
            }

            return rez;
        }

        /*!
         *  Starts recording from web camera.
         *  
         * \param [in] String recPath - path for new video file
         */
        public void startRec(String recPath)
        {
            if (!isDummyWebCam)
            {
                int codec = WebCamHelper.GetCodecFourccCode();
                if (codec > 0)
                {
                    OpenCVCamera.RecordVideo(webCamId, 640, 480, recPath, codec);
                }
                else
                {
                    throw new Exception(PromoTester1.Properties.Resources.badCodecException);
                }

            }
        }

        /*!
         *  Stops recording from web camera.
         */
        public void stopRec()
        {
            if (!isDummyWebCam)
            {
                OpenCVCamera.StopVideoRecording();
            }
        }

        /*!
         *  Starts continuos recording  from web camera. 
         *  This method must be used in the separate thread, immediately after creating new instance of this class.         
         */
        public void record(Object filePath)
        {
            if (!isDummyWebCam)
            {
                try
                {
                    if (!RecStarted)
                    {
                        startRec(filePath.ToString());
                        do
                        {
                            nextImage();
                            writeToAvi();
                        } while (RecStarted);
                    }
                    else
                    {
                        throw new Exception("RecordingError!");
                    }
                }

                catch (WebCamReaderException webCamEx)
                {
                    lock (this)
                    {
                        camOk = false;
                    }
                    //pitaj

                    //Form formX = new Form();
                    //formX.TopMost = true;

                    if (webCamEx.MessageType == WebCamReaderException.WARNING_MESSAGE)
                    {
                        MessageBox.Show( webCamEx.Message, PromoTester1.Properties.Resources.warning_caption, MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else
                    {
                        MessageBox.Show( webCamEx.Message, PromoTester1.Properties.Resources.error_caption, MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                    File.AppendAllText(PromoTester1.Properties.Settings.Default.errorsLogFileName, Helper.GetUnixTimestamp() + " " + webCamEx.Message + " " + webCamEx.StackTrace + Environment.NewLine);
                }

                catch (Exception ex)
                {
                    lock (this)
                    {
                        camOk = false;
                    }

                    MessageBox.Show( ex.Message, PromoTester1.Properties.Resources.error_caption, MessageBoxButton.OK, MessageBoxImage.Error);
                    File.AppendAllText(PromoTester1.Properties.Settings.Default.errorsLogFileName, Helper.GetUnixTimestamp() + " " + ex.Message + " " + ex.StackTrace + Environment.NewLine);
                }
            }
        }

        public void camCheckForever()
        {
            if (!isDummyWebCam)
            {
                lock (this)
                {
                    camCheckStarted = true;
                }

                while (CamCheckStarted)
                {
                    System.Threading.Thread.Sleep(10);                           
                }
            }
        }

        public void stopCamCheck()
        {
            lock (this)
            {
                camCheckStarted = false;
            }
        }

        /*!
         *  Disposes video capture         
         */
        public void closeWebCamStream()
        {
            if (!isDummyWebCam)
            {
                lock (this)
                {
                    OpenCVCamera.disposeCapture();
                    webCamStarted = false;
                }
            }
        }
    }
}
