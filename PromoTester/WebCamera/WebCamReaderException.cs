﻿using System;

namespace PromoTester1
{
    [Serializable()]
    public class WebCamReaderException : Exception
    {
        public const int WARNING_MESSAGE = 0;
        public const int ERROR_MESSAGE = 1;

        private int messageType;
        public int MessageType
        {
            get { return messageType; }
        }

        public WebCamReaderException(string msg, int msgType) : base(msg)
        {
            messageType = msgType;
        }
    }
}
