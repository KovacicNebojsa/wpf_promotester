﻿using PromoTester1.UserControlFolder;
using PromoTester1.WebCamera;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows;
using System.Xml;

namespace PromoTester1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        WebCamReader webCamReader;
        private bool noWebCam;
        private Thread webCamCheckThread;
        public MainWindow()
        {

            InitializeComponent();
            #region Ako zelimo da inicijalizujemo na samom pocetku kameru, trebace za kasnije
            //if (File.Exists("nowebcam.pt"))
            //{
            //    String nowebcam = File.ReadAllText("nowebcam.pt").Trim();
            //    if (nowebcam.CompareTo("true") == 0)
            //    {
            //        noWebCam = true;
            //    }
            //    else
            //    {
            //        noWebCam = false;
            //    }
            //}
            //else
            //{
            //    noWebCam = false;
            //}
            //if (noWebCam)
            //{
            //    webCamReader = new WebCamReader();
            //}
            //else
            //{
            //    webCamReader = new WebCamReader(PromoTester1.Properties.Settings.Default.webCamId);
            //    webCamCheckThread = new Thread(new ThreadStart(webCamReader.camCheckForever));
            //    webCamCheckThread.IsBackground = true;
            //    webCamCheckThread.Start();
            //}
            #endregion
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ShowDialog_Click(object sender, RoutedEventArgs e)
        {
            ProgressBarDialog pg = new ProgressBarDialog(SaveData);
            //ProgressBarDialog pg = new ProgressBarDialog(SaveDataToFolder);
            pg.ShowDialog();
        }

        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {


        }
                       
        private void btnStartTest_Click(object sender, RoutedEventArgs e)
        {
            WebTaskMediaDialog wmd = new WebTaskMediaDialog();
            grid1.Children.Clear();
            grid1.Children.Add(wmd);
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnLoadMain_Click(object sender, RoutedEventArgs e)
        {
        }

        #region Obrisi, Testiranje promjene jezika i USER CONTROLE
        //brisi
        WebTaskMediaDialog wmd = new WebTaskMediaDialog();
        private void ComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (cmbLanguage.SelectedIndex == 0)
            {
               // grid1.Children.Remove(wmd);
                
                //var langCode = PromoTester1.Properties.Settings.Default.languageCode;
                //Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(langCode);
                //Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en");
                //Properties.Settings.Default.languageCode = "en-US";
            }
            else
            {
                 //grid1.Children.Add(wmd);

                //var langCode = PromoTester1.Properties.Settings.Default.languageCodeSr;
                //Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(langCode);
                //Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("sr");
                //Properties.Settings.Default.languageCode = "sr-SR";
            }
            
            //InitializeComponent();
            //Window_Loaded(sender, e);
            //Properties.Settings.Default.Save();
        }
        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void btnWebCamera_Click(object sender, RoutedEventArgs e)
        {
            WebCamReader webCamReader = new WebCamReader(PromoTester1.Properties.Settings.Default.webCamId);
            Thread webCamCheckThread = new Thread(new ThreadStart(webCamReader.camCheckForever));
            webCamCheckThread.IsBackground = true;
            webCamCheckThread.Start();

            WebcamCheckDialog wb = new WebcamCheckDialog(webCamReader);
            //WebCamSelectDialog wb = new WebCamSelectDialog();
            wb.ShowDialog();
        }




        void SaveData()
        {
            for (int i = 0; i < 500; i++)
            {
                Thread.Sleep(20);
            }
        }
        private void btnChooseCamera_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                String video_input_list;
                StringBuilder tmp_video_input_list = new StringBuilder(1000);
                OpenCVCamera.FindVideoInputandList(tmp_video_input_list);

                video_input_list = tmp_video_input_list.ToString();
                String[] webcams = video_input_list.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                if (webcams.Length > 0)
                {
                    WebCamSelectDialog wcsd = new WebCamSelectDialog(webcams);
                    bool? result = wcsd.ShowDialog();
                    if (result == true)
                    {
                        PromoTester1.Properties.Settings.Default.webCamId = wcsd.SelectedWebcamId;
                        PromoTester1.Properties.Settings.Default.Save();
                        webCamReader = new WebCamReader(PromoTester1.Properties.Settings.Default.webCamId);
                        webCamCheckThread = new Thread(new ThreadStart(webCamReader.camCheckForever));
                        webCamCheckThread.IsBackground = true;
                        webCamCheckThread.Start();
                        WebcamCheckDialog wb = new WebcamCheckDialog(webCamReader);
                        wb.ShowDialog();

                    }
                }
                else
                {
                    MessageBox.Show(PromoTester1.Properties.Resources.noWebcamError, PromoTester1.Properties.Resources.error_caption, MessageBoxButton.OK, MessageBoxImage.Error);
                    File.AppendAllText(PromoTester1.Properties.Settings.Default.errorsLogFileName, Helper.GetUnixTimestamp() + " " + PromoTester1.Properties.Resources.noWebcamError + Environment.NewLine);
                }
            }
            catch
            {
                MessageBox.Show(PromoTester1.Properties.Resources.noWebcamError, PromoTester1.Properties.Resources.error_caption, MessageBoxButton.OK, MessageBoxImage.Error);
                File.AppendAllText(PromoTester1.Properties.Settings.Default.errorsLogFileName, Helper.GetUnixTimestamp() + " " + PromoTester1.Properties.Resources.noWebcamError + Environment.NewLine);
            }
        }
    }
}
