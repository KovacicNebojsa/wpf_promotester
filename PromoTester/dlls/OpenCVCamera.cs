﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PromoTester1
{
    public static class OpenCVCamera
    {
        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "GetOpenCVCameraDllVersion", CallingConvention = CallingConvention.Cdecl)]
        public static extern void GetOpenCVCameraDllVersion(StringBuilder version);

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "RecordVideo", CallingConvention = CallingConvention.Cdecl)]
        public static extern void RecordVideo(int camID, int capW, int capH, string filePath, int fourcc);

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "StopVideoRecording", CallingConvention = CallingConvention.Cdecl)]
        public static extern void StopVideoRecording();

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "RecordScreen", CallingConvention = CallingConvention.Cdecl)]
        public static extern void RecordScreen(int screenW, int screenH, int videoW, string filePath, int fourcc);

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "StopScreenRecording", CallingConvention = CallingConvention.Cdecl)]
        public static extern void StopScreenRecording();

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "getCurrentFrame", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr getCurrentFrame();

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "createCaptureDevice", CallingConvention = CallingConvention.Cdecl)]
        public static extern int createCaptureDevice(int cam_id, int capture_w, int capture_h);

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "createCaptureFile", CallingConvention = CallingConvention.Cdecl)]
        public static extern int createCaptureFile(String path, ref int w, ref int h);

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "queryFrame", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr queryFrame(ref int end, int zoom);

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "disposeCapture", CallingConvention = CallingConvention.Cdecl)]
        public static extern void disposeCapture();


        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "createCaptureFileVideo", CallingConvention = CallingConvention.Cdecl)]
        public static extern int createCaptureFileVideo(String path, ref int w, ref int h, ref double fps);

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "queryFrameVideo", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr queryFrameVideo(ref int end);

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "disposeCaptureVideo", CallingConvention = CallingConvention.Cdecl)]
        public static extern void disposeCaptureVideo();

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "camCheck", CallingConvention = CallingConvention.Cdecl)]
        public static extern int camCheck(int cam_id);


        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "createVideoWriter", CallingConvention = CallingConvention.Cdecl)]
        public static extern int createVideoWriter(String file_name, int fourcc, double fps, int sizeW, int sizeH);

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "writeFrame", CallingConvention = CallingConvention.Cdecl)]
        public static extern int writeFrame();

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "writeVideoFrame", CallingConvention = CallingConvention.Cdecl)]
        public static extern int writeVideoFrame(int x, int y);

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "disposeVideoWriter", CallingConvention = CallingConvention.Cdecl)]
        public static extern void disposeVideoWriter();

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "FindVideoInputandList", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int FindVideoInputandList(StringBuilder tmp_video_input_list);

        //[DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "preProcessVideo", CallingConvention = CallingConvention.Cdecl)]
        //public static extern int preProcessVideo(String path, String path1, int s_w, int s_h, int fourcc);

        //snimanje web browser
        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "createVideoWriterVideo", CallingConvention = CallingConvention.Cdecl)]
        public static extern int createVideoWriterVideo(String file_name, int fourcc, double fps, int sizeW, int sizeH, int videoWidth);

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "writeFrameVideo", CallingConvention = CallingConvention.Cdecl)]
        public static extern int writeFrameVideo(ref byte frame);

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "writeDesktopFrame", CallingConvention = CallingConvention.Cdecl)]
        public static extern int writeDesktopFrame();

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "disposeVideoWriterVideo", CallingConvention = CallingConvention.Cdecl)]
        public static extern void disposeVideoWriterVideo();

        [DllImport("opencvCamera\\OpenCVCameradll.dll", EntryPoint = "repackVideo", CallingConvention = CallingConvention.Cdecl)]
        public static extern int repackVideo(String fileName, String fileNameCorrected, int fourcc, int realDuration);
    }
}
