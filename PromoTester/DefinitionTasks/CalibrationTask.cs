﻿using PromoTester1.UserControlFolder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Xml;

namespace PromoTester1
{
    class CalibrationTask
    {
        #region Fields
        private SynchronizationContext synchronizationContext;

        private int screenWidth;
        private int screenHeight;

        private Point calibGridDimension;

        private int calibPointX;
        private int calibPointY;

        private String localPath;
        private String recPath;

        private WebCamReader webCamReader;
        private WebCamReader webCamReaderSync
        {
            get { lock (this) { return webCamReader; } }
        }

        private List<XmlElement> imageElementList;

        private Thread imagesThread;
        private Thread algoThread;

        private bool isOnlineAnalysis;
        private bool analysisStarted;
        private bool analysisStartedSync
        {
            get { lock (this) { return analysisStarted; } }
            set { lock (this) { analysisStarted = value; } }
        }

        private String errMessage;
        public String errorMessage
        {
            get { lock (this) { return errMessage; } }
            set { lock (this) { errMessage = value; } }
        }

        private String warnMessage;
        public String warningMessage
        {
            get { lock (this) { return warnMessage; } }
            set { lock (this) { warnMessage = value; } }
        }

        private bool mouseClicked;
        private bool mouseClickedSync
        {
            get { lock (this) { return mouseClicked; } }
            set { lock (this) { mouseClicked = value; } }
        }

        private bool isClickable;
        public bool isClickableSync
        {
            get { lock (this) { return isClickable; } }
            set { lock (this) { isClickable = value; } }
        }

        private int recVideoFps;
        public int RecVideoFps
        {
            get { lock (this) { return recVideoFps; } }
        }

        private String fpsMessage;
        public String FpsMessage
        {
            get { lock (this) { return fpsMessage; } }
            set { lock (this) { fpsMessage = value; } }
        }

        private DateTime imageSetTime;

        private String insightCode;

        private object sleeper;

        private int log_errors;

        private int credit_count;
        public int CreditCount
        {
            get { return credit_count; }
        }

        //log params:
        public DateTime start_time;
        public double image_quality_start;
        public double image_quality_min;
        public double image_quality_sum;
        public int image_quality_count;
        public double head_movement_max;
        public double head_movement_sum;
        public int head_movement_count;
        public double head_size_ratio_max;
        public double head_size_ratio_sum;
        public int head_size_ratio_count;

        public int _recordVideo;
        public int _recordAudio;
        public int _eyeGaze;
        public int _emotionDetection;
        private List<XmlElement> calibrationImageElementList;
        private string localResultsPath;
        private string outputName;
        private System.Windows.Point gridDimension;
        private bool clickable;
        private bool recordVideo;
        private bool recordAudio;
        private bool eyeGaze;
        private bool emotionDetection;


        WebTaskMediaDialog webTaskMediaDialog;
        string elementName = "";

        #endregion


        public CalibrationTask(WebTaskMediaDialog w, List<XmlElement> calibrationElements)
        {
            this.webTaskMediaDialog = w;

            var imagesThread = new Thread(() =>
            {
                webTaskMediaDialog.PlayImages(calibrationElements);
            });
            imagesThread.IsBackground = true;
            imagesThread.Start();

        }
        //public CalibrationTask(WebTaskMediaDialog webTaskMediaDialog, List<XmlElement> calibrationImageElementList, String path, String resultsPath, String outputName, Point gridDimension, WebCamReader camReader, String insightPass, bool clickable, bool recordVideo, bool recordAudio, bool eyeGaze, bool emotionDetection, int _screenWidth, int _screenHeight)
        //{
        //    this.webTaskMediaDialog = webTaskMediaDialog;
        //    recVideoFps = -1;
        //    credit_count = 0;

        //    sleeper = new object();

        //    synchronizationContext = SynchronizationContext.Current;

        //    screenWidth = _screenWidth;//((InfoMediaDialog)Owner).ScreenWidth;
        //    screenHeight = _screenHeight;//((InfoMediaDialog)Owner).ScreenHeight;

        //    webTaskMediaDialog.chrome.Width = screenWidth;
        //    webTaskMediaDialog.chrome.Height = screenHeight;

        //    localPath = path;
        //    imageElementList = calibrationImageElementList;
        //    webCamReader = camReader;

        //    calibGridDimension = gridDimension;
        //    calibPointX = -1;
        //    calibPointY = -1;

        //    isClickable = clickable;
        //    mouseClicked = false;

        //    isOnlineAnalysis = eyeGaze || emotionDetection || recordVideo;
        //    recPath = resultsPath + "\\" + outputName + ".avi";

        //    _recordVideo = 0;
        //    _recordAudio = 0;
        //    _eyeGaze = 0;
        //    _emotionDetection = 0;
        //    if (recordVideo)
        //        _recordVideo = 1;
        //    if (recordAudio)
        //        _recordAudio = 1;
        //    if (eyeGaze)
        //        _eyeGaze = 1;
        //    if (emotionDetection)
        //        _emotionDetection = 1;

        //    errMessage = "";
        //    warnMessage = "";
        //    fpsMessage = "";
        //    imageSetTime = DateTime.MinValue;
        //    //DialogResult = DialogResult.OK;

        //    insightCode = insightPass;

        //    log_errors = PromoTester1.Properties.Settings.Default.debug_mode ? 1 : 0;

        //    start_time = DateTime.Now;
        //    image_quality_start = 0;
        //    image_quality_min = 1000;
        //    image_quality_sum = 0;
        //    image_quality_count = 0;
        //    head_movement_max = 0;
        //    head_movement_sum = 0;
        //    head_movement_count = 0;
        //    head_size_ratio_max = 0;
        //    head_size_ratio_sum = 0;
        //    head_size_ratio_count = 0;
        //    //calibrationImageElementList.RemoveAt(0);
        //    //playImages(calibrationImageElementList);
        //}

        #region Brisi
        //public CalibrationTask(WebTaskMediaDialog w, List<XmlElement> calibrationImageElementList, string localPath, string localResultsPath, string outputName, System.Windows.Point gridDimension, WebCamReader webCamReader, string insightCode, bool clickable, bool recordVideo, bool recordAudio, bool eyeGaze, bool emotionDetection, int screenWidth, int screenHeight)
        //{
        //    webTaskMediaDialog = w;
        //    this.calibrationImageElementList = calibrationImageElementList;
        //    this.localPath = localPath;
        //    this.localResultsPath = localResultsPath;
        //    this.outputName = outputName;
        //    this.gridDimension = gridDimension;
        //    this.webCamReader = webCamReader;
        //    this.insightCode = insightCode;
        //    this.clickable = clickable;
        //    this.recordVideo = recordVideo;
        //    this.recordAudio = recordAudio;
        //    this.eyeGaze = eyeGaze;
        //    this.emotionDetection = emotionDetection;
        //    this.screenWidth = screenWidth;
        //    this.screenHeight = screenHeight;

          
        //}
        #endregion

        private void playImages(Object xmlList)
        {
            Bitmap calibImg = new Bitmap(screenWidth, screenHeight);
            Graphics g = Graphics.FromImage(calibImg);

            List<XmlElement> imgElementList = (List<XmlElement>)xmlList;
            System.Drawing.Image img = null;
            int timeout = 0;

            try
            {
                int i = 0;
                bool isAnalysisOk = true;
                while ((webCamReaderSync.CamOk) && (i < imgElementList.Count) && isAnalysisOk)
                {
                    g.FillRectangle(Brushes.White, new Rectangle(0, 0, screenWidth, screenHeight));

                    img = processCalibrationImageElement(imgElementList[i], out timeout);
                    g.DrawImage(img, calibPointX - img.Width / 2, calibPointY - img.Height / 2);

                    //synchronizationContext.Send(SetImage, calibImg);

                    if (isClickableSync)
                    {
                        lock (sleeper)
                        {
                            Monitor.Wait(sleeper);
                        }
                    }
                    else
                    {
                        lock (this)
                        {
                            imageSetTime = DateTime.Now;
                        }
                        lock (sleeper)
                        {
                            Monitor.Wait(sleeper, timeout);
                        }
                    }

                    if (isOnlineAnalysis)
                    {
                        if (analysisStartedSync)
                        {
                            isAnalysisOk = true;
                        }
                        else
                        {
                            isAnalysisOk = false;
                        }
                    }

                    //i++;
                }

                if (!webCamReaderSync.CamOk)
                {
                    errorMessage += Environment.NewLine + PromoTester1.Properties.Resources.cameraDisconnectedError;
                }
            }

            catch (Exception ex)
            {
                errorMessage += Environment.NewLine + ex.Message; // + Environment.NewLine + "    " + ex.StackTrace;                
            }

            finally
            {
                g.Dispose();
                if (isOnlineAnalysis)
                {
                    analysisStartedSync = false;
                    //Console.WriteLine("Zavrsio slajdshow!");
                }
                else
                {
                    webCamReaderSync.stopRec();
                    //synchronizationContext.Send(CloseForm, null);
                }

            }
        }
        private System.Drawing.Image processCalibrationImageElement(XmlElement calibrationImageElement, out int timeout)
        {
            if ((calibrationImageElement.Name.Trim().CompareTo("calibrationImage") == 0))
            {
                try
                {
                    String src = calibrationImageElement.GetAttribute("src");
                    System.Drawing.Image img = Image.FromStream(new MemoryStream(File.ReadAllBytes(localPath + "\\" + src)));
                    timeout = (int.Parse(calibrationImageElement.GetAttribute("timeout"))) * 1000;

                    if (calibrationImageElement.HasAttribute("calibPointX") && calibrationImageElement.HasAttribute("calibPointY"))
                    {
                        int x = int.Parse(calibrationImageElement.GetAttribute("calibPointX"));
                        int y = int.Parse(calibrationImageElement.GetAttribute("calibPointY"));

                        Point calibPoint = calculateCalibrationPoint(x, y);
                        calibPointX = calibPoint.X;
                        calibPointY = calibPoint.Y;
                    }

                    return img;
                }
                catch (Exception)
                {
                    throw new XmlException(PromoTester1.Properties.Resources.xmlException_calib_attr);
                }
            }
            else
            {
                throw new XmlException(PromoTester1.Properties.Resources.xmlException_calib_elem);
            }
        }
        private Point calculateCalibrationPoint(int x, int y)
        {
            int gridCellW = (int)webTaskMediaDialog.grid2.RenderSize.Width / calibGridDimension.X;
            int gridCellH = (int)webTaskMediaDialog.grid2.RenderSize.Height/ calibGridDimension.Y;

            int deltaW = gridCellW / 2;
            int deltaH = gridCellH / 2;

            return new Point(gridCellW * x + deltaW, gridCellH * y + deltaH);
        }

    }
}
