﻿using PromoTester1.UserControlFolder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace PromoTester1
{
    public class WatchTaskElement
    {

        WebTaskMediaDialog webTaskMediaDialog;
        string elementName = "";
        private String localPath;



        public WatchTaskElement(WebTaskMediaDialog w, List<XmlElement> watchtaskElements)
        {
            this.webTaskMediaDialog = w;

            var imagesThread = new Thread(() =>
            {
                webTaskMediaDialog.PlayImages(watchtaskElements);
            });
            imagesThread.IsBackground = true;
            imagesThread.Start();

        }
    }
}
