﻿using PromoTester1.UserControlFolder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace PromoTester1
{
    public class TestCalibrationTask
    {

        WebTaskMediaDialog webTaskMediaDialog;
        string elementName = "";
        private String localPath;



        public TestCalibrationTask(WebTaskMediaDialog w, List<XmlElement> testCalibrationElements)
        {
            this.webTaskMediaDialog = w;

            var imagesThread = new Thread(() =>
            {
                webTaskMediaDialog.PlayImages(testCalibrationElements);
            });
            imagesThread.IsBackground = true;
            imagesThread.Start();

        }

    }
}
