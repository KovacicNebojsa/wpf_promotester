﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Xml;

namespace PromoTester1.UserControlFolder
{
    /// <summary>
    /// Interaction logic for WatchTask.xaml
    /// </summary>
    public partial class WatchTask : UserControl
    {
        SynchronizationContext synchronizationContext = null;
        private List<XmlElement> imageElementList = null;
        private XmlElement watchtaskElement = null;
        private List<XmlElement> watchTaskList = null;
        private string localPath = @"C:\Users\Nebojsa\Documents\PT\PromoTester\bin\Release\PROMOTESTER_REPOSITORY\71751909\x_protech_proba_5491\";
        public WatchTask()
        {
            InitializeComponent();
            synchronizationContext = SynchronizationContext.Current;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load($"{localPath}definition.xml");
            imageElementList = readXmlElements(xmlDoc.DocumentElement);
            //getNextXmlElement();
            GetWatchTask();
        }
        private List<XmlElement> readXmlElements(XmlNode element)
        {
            List<XmlElement> xmlElements = new List<XmlElement>();

            if (element.HasChildNodes)
            {
                for (int i = 0; i < element.ChildNodes.Count; i++)
                {
                    XmlNode node = element.ChildNodes[i];

                    if (node.NodeType == XmlNodeType.Element)
                    {
                        xmlElements.Add(node as XmlElement);
                    }
                }
            }
            if (xmlElements.Count != 0)
            {
                Directory.CreateDirectory($"{localPath}test1");
                return xmlElements;
            }
            else
            {
                return null;
            }
            
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var imagesThread = new Thread(new ParameterizedThreadStart(playImages));
            imagesThread.IsBackground = true;
            imagesThread.Start(imageElementList);
           
        }
        private void playImages(object xmlList)
        {
            // Uri fileUri = new Uri("C:\\Users\\Nebojsa\\Documents\\PT\\PromoTester\\bin\\Release\\PROMOTESTER_REPOSITORY\\71751909\\x_protech_proba_5491" + "\\" + src, UriKind.RelativeOrAbsolute);
            int timeout = 0;
            for (int i = 0; i < watchTaskList.Count; i++)
            {
                var uri = processWatchscreenElement(watchTaskList[i], out timeout);
                synchronizationContext.Send((a) =>
                {
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.UriSource = uri;
                    bitmap.EndInit();
                    imgOriginalSize.Source = bitmap; 
                    
                }, null);
                Thread.Sleep(timeout);
               
            }
            synchronizationContext.Send((a) =>
            {
                MainWindow mw = new MainWindow();
                Window.GetWindow(this).Close();
                mw.ShowDialog();
            }, null);
        }
        //private XmlElement getNextXmlElement()
        //{
        //    watchtaskElement = (XmlElement)imageElementList[1];

        //    return watchtaskElement;
        //}
        private List<XmlElement> GetWatchTask()
        {
            if (watchtaskElement.Name.Trim().CompareTo("watchtask") == 0)
            {
                if (watchtaskElement.HasAttribute("output"))
                {
                    if (watchtaskElement.HasChildNodes)
                    {
                        watchTaskList = readXmlElements(watchtaskElement);

                        return watchTaskList;

                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        private Uri processWatchscreenElement(XmlElement watchtaskImageElement, out int timeout)
        {
            if ((watchtaskImageElement.Name.Trim().CompareTo("watchscreen") == 0))
            {
                try
                {
                    String src = watchtaskImageElement.GetAttribute("src");
                    Uri fileUri = new Uri("C:\\Users\\Nebojsa\\Documents\\PT\\PromoTester\\bin\\Release\\PROMOTESTER_REPOSITORY\\71751909\\x_protech_proba_5491" + "\\" + src, UriKind.RelativeOrAbsolute);
                    timeout = (int.Parse(watchtaskImageElement.GetAttribute("timeout"))) * 1000;
                    return fileUri;
                }
                catch (Exception ex)
                {
                    throw new XmlException(ex.Message);
                }
            }
            else
            {
                throw new XmlException();
            }
        }
    }
}
