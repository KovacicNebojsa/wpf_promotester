﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace PromoTester1.UserControlFolder
{
    /// <summary>
    /// Interaction logic for VideoWeb.xaml
    /// </summary>
    public partial class VideoWeb : UserControl
    {
        //private string localPath = @"C:\Users\Nebojsa\Documents\PT\PromoTester\bin\Release\PROMOTESTER_REPOSITORY\71751909\x_protech_proba_5491\test1\index.html";

        public VideoWeb()
        {
            //chrome.Address = $"file:///{test1Path}{index}";
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            webBrowserVide.Source = new Uri($"https://www.youtube.com/watch?v=akU0srKozdQ&list=RD7fV6fmBerRg&index=22&ab_channel=StevieElizabeth475");
            //webBrowserVide.Source = new Uri("file:///C:/Users/Nebojsa/Documents/PT/PromoTester/bin/Release/PROMOTESTER_REPOSITORY/71751909/x_protech_proba_5491/images/indexVideo.html");

        }
    }
}
