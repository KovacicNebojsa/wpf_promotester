﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

using System.Xml;

namespace PromoTester1.UserControlFolder
{
    public partial class WebTaskMediaDialog : UserControl
    {
        #region Fields
        SynchronizationContext synchronizationContext = null;
        XmlDocument xmlDoc = null;
        private List<XmlElement> allElementsInDefinition = null;

        private List<XmlElement> infoElemets = null;
        private List<XmlElement> calibrationElements = null;
        private List<XmlElement> testCalibrationElements = null;
        private List<XmlElement> watchtaskElements = null;
        private List<XmlElement> clicktaskElements = null;
        private List<XmlElement> webtaskElements = null;

        public static string repositoryPath = Directory.GetCurrentDirectory();
        private string localPath = $"{repositoryPath}\\PROMOTESTER_REPOSITORY\\71751909\\x_protech_proba_5491\\";
        private string localResultsPath = $"{repositoryPath}\\PROMOTESTER_REPOSITORY\\71751909\\x_protech_proba_5491\\rec\\";
        private string test1Path = $"{repositoryPath}\\PROMOTESTER_REPOSITORY\\71751909\\x_protech_proba_5491\\test1\\";
        public string index = "index";
        public string nebojsa = "";
        private bool noWebCam = false;
        WebCamReader webCamReader = null;
        public bool clicked = false;
        public static int i = 0;
        private bool isCalibrationClickable;
        private bool isCalibrationOnline;
        #endregion

        #region Constructor/s
        public WebTaskMediaDialog()
        {
            InitializeComponent();
            infoElemets = new List<XmlElement>();
            calibrationElements = new List<XmlElement>();
            testCalibrationElements = new List<XmlElement>();
            watchtaskElements = new List<XmlElement>();
            clicktaskElements = new List<XmlElement>();
            webtaskElements = new List<XmlElement>();
            chrome.Address = $"file:///{test1Path}{index}";
            //chrome.Address = $"file:///C:\\Users\\Nebojsa\\Documents\\PT\\PromoTester\\bin\\Release\\PROMOTESTER_REPOSITORY\\71751909\\x_protech_proba_5491\\test1\\calibrationInfo_intro08-nonclick-en.jpg.html";
            synchronizationContext = SynchronizationContext.Current;
            xmlDoc = new XmlDocument();
            xmlDoc.Load($"{localPath}definition.xml");
            Directory.CreateDirectory($"{localPath}test1");
            if (File.Exists($"{test1Path}index.html"))
            {
                File.Delete($"{test1Path}index.html");
            }
            File.AppendAllText($"{test1Path}index.html", @"<!DOCTYPE html>
<html>
<head>
    <title>EyeeSee</title>
</head>
<body>
    <h1>Welcome (71751909) to EyeeSee Testing</h1>
    <h2>Press START to Begin test</h2>
</body>
</html>");
            allElementsInDefinition = Helper.ReadAllXmlElements(xmlDoc.DocumentElement);
            Helper.GenerateHtmlForAllElements(allElementsInDefinition, test1Path);
            Helper.SeperateElements(allElementsInDefinition, ref infoElemets, ref calibrationElements, ref testCalibrationElements, ref watchtaskElements, ref clicktaskElements, ref webtaskElements);
            CalibrationHelper.GeneratePointsOnCanvas(test1Path, calibrationElements);
            TestCalibrationHelper.GeneratePointsOnCanvas(test1Path, testCalibrationElements);

            #region Camera
            if (File.Exists("nowebcam.pt"))
            {
                String nowebcam = File.ReadAllText("nowebcam.pt").Trim();
                if (nowebcam.CompareTo("true") == 0)
                {
                    noWebCam = true;
                }
                else
                {
                    noWebCam = false;
                }
            }
            else
            {
                noWebCam = false;
            }
            if (noWebCam)
            {
                webCamReader = new WebCamReader();
            }
            else
            {
                webCamReader = new WebCamReader(PromoTester1.Properties.Settings.Default.webCamId);
            }
            #endregion
        }
        #endregion

        #region UiMethods
        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            // CalibrationTask a = new CalibrationTask(this);
            txtboxLink.Text = i.ToString();
            if (i >= 0)
            {
                --i;
                var elementName = GetNextXmlElementName(allElementsInDefinition, ref i);
                if (!elementName.Contains("done"))
                {
                    DisplayToChromium(elementName);
                    txtboxLink.Text = i.ToString();
                }
            }
            if (i == -1)
            {
                i++;
            }

        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            txtboxLink.Text = i.ToString();

            if (allElementsInDefinition[i].Name.Trim().CompareTo("info") == 0)
            {
                i++;
                var elementName = GetNextXmlElementName(allElementsInDefinition, ref i);
                GetNextXmlElement(allElementsInDefinition, ref i);
                if (!elementName.Contains("done"))
                {
                    DisplayToChromium(elementName);
                    txtboxLink.Text = i.ToString();
                }
                else
                {
                    DisplayToChromium(index);
                    btnNext.IsEnabled = false;
                    btnStart.IsEnabled = false;
                    btnPrev.IsEnabled = false;
                    txtboxLink.Text = "Done";
                }
            }
            lock (this)
            {
                clicked = true;
                Monitor.Pulse(this);
            }
            clicked = false;

        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (i == 0)
            {
                //WebCamHelper.CheckForCameraAndStart(webCamReader);
                
                txtboxLink.Text = i.ToString();
                var elementName = GetNextXmlElementName(allElementsInDefinition, ref i);
                DisplayToChromium(elementName);
                i++;
                txtboxLink.Text = i.ToString();
            }

        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }

        //trenutno ne radi nista
        private void btnTest_Click(object sender, RoutedEventArgs e)
        {



            var imagesThread = new Thread(() =>
            {
                PlayImages(calibrationElements);
            });
            imagesThread.IsBackground = true;
            imagesThread.Start();

            //Grid.SetRow(grid2, 0);
            //Grid.SetRowSpan(grid2, 2);
            //grid1.Visibility = Visibility.Hidden;
            chrome.Address = @"file:///C:/Users/Nebojsa/Documents/PT/PromoTester/bin/Release/PROMOTESTER_REPOSITORY/71751909/x_protech_proba_5491/test1/calibration_C1.html";
            //CalibrationTask c = new CalibrationTask(this);

            String outputName = "";
            System.Drawing.Point gridDimension = new System.Drawing.Point(0, 0);
            bool clickable = false;
            bool onlineAnalysis = false;
            bool shuffle = false;
            bool recordVideo = false;
            bool recordAudio = false;
            bool eyeGaze = false;
            bool emotionDetection = false;
            bool faceLandmarks = false;

            var element = GetNextXmlElement(allElementsInDefinition, ref i);
            List<XmlElement> calibrationImageElementList = processCalibrationElement(element, out outputName, out gridDimension, out clickable, out onlineAnalysis, out recordVideo, out recordAudio, out eyeGaze, out emotionDetection, out faceLandmarks, out shuffle);
            isCalibrationClickable = clickable;
            isCalibrationOnline = onlineAnalysis;
            //if ((outputName.CompareTo("calibration_main") == 0) && (startTaskName.Length > 0))
            //{
            //    outputName += "_" + startTaskName;
            //}

            //string calibration_Name = element.Attributes["output"].Value;
            //if (log_errors == 1)
            //{
            //    File.AppendAllText(sequenceLogFilePath, Form1.getUnixTimestamp() + ";" + "Calibration " + outputName + " started..." + Environment.NewLine);
            //}
            //File.AppendAllText(SequenceFilePath, "CALIBRATION" + Environment.NewLine);

            //if (calibrationImageElementList != null)
            //{


            int ScreenWidth = 1920;
            int ScreenHeight = 1080;
            string insightCode = "e512ce3552464de7b7f01c14bfd16b2c";
            //Point gridDimension = new Point(0,0);


            // CalibrationTask cmd = new CalibrationTask(this,calibrationImageElementList, localPath, localResultsPath, outputName, gridDimension, webCamReader, insightCode, clickable, recordVideo, recordAudio, eyeGaze, emotionDetection, ScreenWidth, ScreenHeight);

            //if (clickable)
            //{
            //    cmd.Cursor = Cursors.Cross;
            //}
            //else
            //{
            //    Cursor.Hide();
            //}

            //cmd.ShowDialog(this); //pusta slike

            //if (clickable)
            //{
            //    cmd.Cursor = Cursors.Default;
            //}
            //else
            //{
            //    Cursor.Show();
            //}

            //CalibrationFPS = cmd.RecVideoFps;
            //if (cmd.FpsMessage.Length > 0)
            //{
            //    File.AppendAllText(mainLog, Form1.getUnixTimestamp() + " " + cmd.FpsMessage + Environment.NewLine);
            //}

            //if (cmd.errorMessage.Length > 0)
            //{
            //    MessageBox.Show(this, cmd.errorMessage, PromoTester.Properties.Resources.error_caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    File.AppendAllText(errorsLog, Form1.getUnixTimestamp() + " " + cmd.errorMessage + Environment.NewLine);

            //    //*** do log!!!
            //    cmd.logCalibration(mainLog, outputName, cmd.start_time, cmd.image_quality_start, cmd.image_quality_min, cmd.image_quality_sum, cmd.image_quality_count, cmd.head_movement_max, cmd.head_movement_sum, cmd.head_movement_count, cmd.head_size_ratio_max, cmd.head_size_ratio_sum, cmd.head_size_ratio_count, 999999, 999999, cmd.errorMessage);
            //    UniversalTestRdr.IncCreditCount(testId, cmd.CreditCount);
            //    cmd.Dispose();
            //    //basicUserAttemptCount++;
            //    if (log_errors == 1)
            //    {
            //        File.AppendAllText(sequenceLogFilePath, Form1.getUnixTimestamp() + ";" + "Calibration " + outputName + " failed!" + Environment.NewLine);
            //    }
            //    return false;
            //}
            //    else if (cmd.warningMessage.Length > 0)
            //    {
            //        MessageBox.Show(this, cmd.warningMessage, PromoTester.Properties.Resources.warning_caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        File.AppendAllText(warningsLog, Form1.getUnixTimestamp() + " " + cmd.warningMessage + Environment.NewLine);

            //        //****do log!!!
            //        cmd.logCalibration(mainLog, outputName, cmd.start_time, cmd.image_quality_start, cmd.image_quality_min, cmd.image_quality_sum, cmd.image_quality_count, cmd.head_movement_max, cmd.head_movement_sum, cmd.head_movement_count, cmd.head_size_ratio_max, cmd.head_size_ratio_sum, cmd.head_size_ratio_count, 999999, 999999, cmd.warningMessage);
            //        UniversalTestRdr.IncCreditCount(testId, cmd.CreditCount);
            //        cmd.Dispose();
            //        //basicUserAttemptCount++;
            //        if (log_errors == 1)
            //        {
            //            File.AppendAllText(sequenceLogFilePath, Form1.getUnixTimestamp() + ";" + "Calibration " + outputName + " failed!" + Environment.NewLine);
            //        }
            //        return false;
            //    }
            //    else
            //    {
            //        ProgressBarDialog doCalibrateProgressBarDialog = new ProgressBarDialog();
            //        if (doCalibrateProgressBarDialog.ShowDialog(this) == DialogResult.Abort)
            //        {
            //            //****do log!!!
            //            cmd.logCalibration(mainLog, outputName, cmd.start_time, cmd.image_quality_start, cmd.image_quality_min, cmd.image_quality_sum, cmd.image_quality_count, cmd.head_movement_max, cmd.head_movement_sum, cmd.head_movement_count, cmd.head_size_ratio_max, cmd.head_size_ratio_sum, cmd.head_size_ratio_count, doCalibrateProgressBarDialog.calib_RE_mean, doCalibrateProgressBarDialog.calib_RE_outliers_count, "no error");
            //            doCalibrateProgressBarDialog.Dispose();
            //            Thread.Sleep(10);
            //            UniversalTestRdr.IncCreditCount(testId, cmd.CreditCount);
            //            cmd.Dispose();
            //            if (log_errors == 1)
            //            {
            //                File.AppendAllText(sequenceLogFilePath, Form1.getUnixTimestamp() + ";" + "Calibration " + outputName + " failed!" + Environment.NewLine);
            //            }
            //            return false;
            //        }
            //        else
            //        {
            //            //****do log!!!
            //            cmd.logCalibration(mainLog, outputName, cmd.start_time, cmd.image_quality_start, cmd.image_quality_min, cmd.image_quality_sum, cmd.image_quality_count, cmd.head_movement_max, cmd.head_movement_sum, cmd.head_movement_count, cmd.head_size_ratio_max, cmd.head_size_ratio_sum, cmd.head_size_ratio_count, doCalibrateProgressBarDialog.calib_RE_mean, doCalibrateProgressBarDialog.calib_RE_outliers_count, "no error");
            //            doCalibrateProgressBarDialog.Dispose();
            //            Thread.Sleep(10);
            //            UniversalTestRdr.IncCreditCount(testId, cmd.CreditCount);
            //            cmd.Dispose();

            //            File.AppendAllText(SequenceFilePath, "CALIBRATION" + Environment.NewLine);
            //            if (log_errors == 1)
            //            {
            //                File.AppendAllText(sequenceLogFilePath, Form1.getUnixTimestamp() + ";" + "Calibration " + outputName + " succeeded!" + Environment.NewLine);
            //            }
            //            return true;
            //        }
            //    }
            //}
            //else
            //{
            //    if (log_errors == 1)
            //    {
            //        File.AppendAllText(sequenceLogFilePath, Form1.getUnixTimestamp() + ";" + "Calibration " + outputName + " failed!" + Environment.NewLine);
            //    }
            //    throw new XmlException(PromoTester.Properties.Resources.xmlException_calibration);
            //}
        }

        #region za brisanje
        private void btnPlayVideo_Click(object sender, RoutedEventArgs e)
        {
           
        }
        #endregion

        #endregion

        #region HelperMethods
        public void PlayImages(List<XmlElement> elementsToDisplay)//ne moze u helper clasu zbog UI elemenata
        {
            int timeout = 0;
            string nameOfElement;
            string mainElement = "";
            for (int j = 0; j < elementsToDisplay.Count; j++)
            {
                if (elementsToDisplay[j].HasChildNodes)
                {

                    mainElement = elementsToDisplay[j].GetAttribute("info");
                    var result = mainElement.Split('/');
                    mainElement = result[1];
                    synchronizationContext.Send((a) =>
                    {
                        Grid.SetRow(grid2, 1);
                        Grid.SetRowSpan(grid2, 1);
                        grid1.Visibility = Visibility.Visible;
                        chrome.Address = $"file:///{localPath}\\test1\\{elementsToDisplay[j].Name}Info_{mainElement}.html";
                    }, null);
                    lock (this)
                    {
                        Monitor.Wait(this, 60000, clicked);
                    }
                }

                else
                {
                    timeout = (int.Parse(elementsToDisplay[j].GetAttribute("timeout"))) * 1000;
                    nameOfElement = elementsToDisplay[j].GetAttribute("output");
                    synchronizationContext.Send((a) =>
                    {
                        Grid.SetRow(grid2, 0);
                        Grid.SetRowSpan(grid2, 2);
                        grid1.Visibility = Visibility.Hidden;
                        chrome.Address = $"file:///{localPath}\\test1\\{elementsToDisplay[j].Name}_{nameOfElement}.jpg.html";
                    }, null);
                    Thread.Sleep(timeout);
                }
            }
            synchronizationContext.Send((a) =>
            {
                i++;
                Grid.SetRow(grid2, 1);
                Grid.SetRowSpan(grid2, 1);
                grid1.Visibility = Visibility.Visible;
                GetNextXmlElement(allElementsInDefinition, ref i);

                if (allElementsInDefinition[i].Name.Trim().CompareTo("info") == 0)
                {
                    var elementName = GetNextXmlElementName(allElementsInDefinition, ref i);
                    GetNextXmlElement(allElementsInDefinition, ref i);
                    DisplayToChromium(elementName);
                }
            }, null);
        }
        public void DisplayToChromium(string nameOfFile)
        {
            chrome.Address = $"file:///{test1Path}{nameOfFile}.html";
        }

        public string GetNextXmlElementName(List<XmlElement> allElementsForOneTask, ref int currentXmlElementIndex)
        {

            var elementName = "";
            if (allElementsForOneTask.Count > currentXmlElementIndex && currentXmlElementIndex >= 0)
            {
                if (allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("info") == 0)
                {
                    elementName = Helper.TakeOnlyElementName(allElementsForOneTask[currentXmlElementIndex], "src");

                    return "info_" + elementName;
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("calibration") == 0))
                {
                    elementName = Helper.TakeOnlyElementName(allElementsForOneTask[currentXmlElementIndex], "info");

                    return "calibration_" + elementName;
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("testcalibration") == 0))
                {
                    elementName = Helper.TakeOnlyElementName(allElementsForOneTask[currentXmlElementIndex], "info");

                    return "testcalibration_" + elementName;
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("watchtask") == 0))
                {
                    elementName = Helper.TakeOnlyElementName(allElementsForOneTask[currentXmlElementIndex], "info");
                    return "watchtaskInfo_" + elementName;
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("clicktask") == 0))
                {
                    elementName = Helper.TakeOnlyElementName(allElementsForOneTask[currentXmlElementIndex], "info");
                    return "clicktaskInfo_" + elementName;
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("webtask") == 0))
                {
                    elementName = Helper.TakeOnlyElementName(allElementsForOneTask[currentXmlElementIndex], "info");
                    return "webtaskInfo_" + elementName;
                }
                else
                {
                    return "";
                }
            }
            //last child element (finish test)
            else
            {
                return "done";
            }
        }
        public XmlElement GetNextXmlElement(List<XmlElement> allElementsForOneTask, ref int currentXmlElementIndex)
        {

            if (allElementsForOneTask.Count > currentXmlElementIndex && currentXmlElementIndex >= 0)
            {
                if (allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("info") == 0)
                {
                    return allElementsForOneTask[currentXmlElementIndex];
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("calibration") == 0))
                {
                    WebCamHelper.CheckForCameraAndStart(webCamReader);
                    CalibrationTask t1 = new CalibrationTask(this, calibrationElements);
                    return allElementsForOneTask[currentXmlElementIndex];
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("testcalibration") == 0))
                {
                    TestCalibrationTask t1 = new TestCalibrationTask(this, testCalibrationElements);
                    return allElementsForOneTask[currentXmlElementIndex];

                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("watchtask") == 0))
                {
                    WatchTaskElement w = new WatchTaskElement(this, watchtaskElements);
                    return allElementsForOneTask[currentXmlElementIndex];
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("clicktask") == 0))
                {
                    //Dodati logiku
                    //ClickTaskElement c = new ClickTaskElement(this,clicktaskElements);
                    return allElementsForOneTask[currentXmlElementIndex];
                }
                else if ((allElementsForOneTask[currentXmlElementIndex].Name.Trim().CompareTo("webtask") == 0))
                {

                    return allElementsForOneTask[currentXmlElementIndex];
                }
                else
                {
                    return allElementsForOneTask[currentXmlElementIndex];
                }
            }
            else
            {
                return null;
            }
        }

        private List<XmlElement> processCalibrationElement(XmlElement calibrationElement, out String output, out System.Drawing.Point gridDimension, out bool clickable, out bool onlineAnalysis, out bool recordVideo, out bool recordAudio, out bool eyeGaze, out bool emotionDetection, out bool faceLandmarks, out bool shuffle)
        {
            if (calibrationElement.HasAttribute("output") && calibrationElement.HasAttribute("gridW") && calibrationElement.HasAttribute("gridH"))
            {
                output = calibrationElement.Attributes["output"].Value;
                gridDimension = new System.Drawing.Point(0, 0);
                gridDimension.X = int.Parse(calibrationElement.Attributes["gridW"].Value);
                gridDimension.Y = int.Parse(calibrationElement.Attributes["gridH"].Value);
                clickable = calibrationElement.HasAttribute("clickable") && (calibrationElement.GetAttribute("clickable").Trim().CompareTo("true") == 0);
                onlineAnalysis = calibrationElement.HasAttribute("online_analysis") && (calibrationElement.GetAttribute("online_analysis").Trim().CompareTo("true") == 0);
                recordVideo = calibrationElement.HasAttribute("record_video") && (calibrationElement.GetAttribute("record_video").Trim().CompareTo("true") == 0);
                recordAudio = calibrationElement.HasAttribute("record_audio") && (calibrationElement.GetAttribute("record_audio").Trim().CompareTo("true") == 0);
                eyeGaze = calibrationElement.HasAttribute("eye_gaze") && (calibrationElement.GetAttribute("eye_gaze").Trim().CompareTo("true") == 0);
                emotionDetection = calibrationElement.HasAttribute("emotion_detection") && (calibrationElement.GetAttribute("emotion_detection").Trim().CompareTo("true") == 0);
                faceLandmarks = calibrationElement.HasAttribute("face_landmarks") && (calibrationElement.GetAttribute("face_landmarkss").Trim().CompareTo("true") == 0);

                shuffle = calibrationElement.HasAttribute("shuffle") && (calibrationElement.GetAttribute("shuffle").Trim().CompareTo("true") == 0);

                if (calibrationElement.HasChildNodes)
                {
                    List<XmlElement> rezList = calibrationElements;
                    if (shuffle)
                    {
                        return rezList;
                        //return shuffleList(rezList);
                    }
                    else
                    {
                        return rezList;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                output = "";
                gridDimension = new System.Drawing.Point(0, 0);
                clickable = false;
                onlineAnalysis = false;
                shuffle = false;
                recordVideo = false;
                recordAudio = false;
                eyeGaze = false;
                emotionDetection = false;
                faceLandmarks = false;

                return null;
            }
        }
        #endregion  

    }
}
