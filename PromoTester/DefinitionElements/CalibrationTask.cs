﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace PromoTester1.DefinitionElements
{
    public class CalibrationTask
    {
        #region Fields
        private SynchronizationContext synchronizationContext;

        private int screenWidth;
        private int screenHeight;

        private Point calibGridDimension;

        private int calibPointX;
        private int calibPointY;

        private String localPath;
        private String recPath;

        private WebCamReader webCamReader;
        private WebCamReader webCamReaderSync
        {
            get { lock (this) { return webCamReader; } }
        }

        private List<XmlElement> imageElementList;

        private Thread imagesThread;
        private Thread algoThread;

        private bool isOnlineAnalysis;
        private bool analysisStarted;
        private bool analysisStartedSync
        {
            get { lock (this) { return analysisStarted; } }
            set { lock (this) { analysisStarted = value; } }
        }

        private String errMessage;
        public String errorMessage
        {
            get { lock (this) { return errMessage; } }
            set { lock (this) { errMessage = value; } }
        }

        private String warnMessage;
        public String warningMessage
        {
            get { lock (this) { return warnMessage; } }
            set { lock (this) { warnMessage = value; } }
        }

        private bool mouseClicked;
        private bool mouseClickedSync
        {
            get { lock (this) { return mouseClicked; } }
            set { lock (this) { mouseClicked = value; } }
        }

        private bool isClickable;
        public bool isClickableSync
        {
            get { lock (this) { return isClickable; } }
            set { lock (this) { isClickable = value; } }
        }

        private int recVideoFps;
        public int RecVideoFps
        {
            get { lock (this) { return recVideoFps; } }
        }

        private String fpsMessage;
        public String FpsMessage
        {
            get { lock (this) { return fpsMessage; } }
            set { lock (this) { fpsMessage = value; } }
        }

        private DateTime imageSetTime;

        private String insightCode;

        private object sleeper;

        private int log_errors;

        private int credit_count;
        public int CreditCount
        {
            get { return credit_count; }
        }

        //log params:
        public DateTime start_time;
        public double image_quality_start;
        public double image_quality_min;
        public double image_quality_sum;
        public int image_quality_count;
        public double head_movement_max;
        public double head_movement_sum;
        public int head_movement_count;
        public double head_size_ratio_max;
        public double head_size_ratio_sum;
        public int head_size_ratio_count;

        public int _recordVideo;
        public int _recordAudio;
        public int _eyeGaze;
        public int _emotionDetection;
        #endregion

        #region Constructor
        public CalibrationTask(List<XmlElement> images, String path, String resultsPath, String outputName, Point gridDimension, WebCamReader camReader, String insightPass, bool clickable, bool recordVideo, bool recordAudio, bool eyeGaze, bool emotionDetection, int _screenWidth, int _screenHeight)
        {

            recVideoFps = -1;
            credit_count = 0;

            sleeper = new object();

            synchronizationContext = SynchronizationContext.Current;

            //bilo
            //screenWidth = InfoMediaDialog.getScreenWidth4(Screen.FromControl(this).Bounds.Width);
            //screenHeight = Screen.FromControl(this).Bounds.Height;
            screenWidth = _screenWidth;//((InfoMediaDialog)Owner).ScreenWidth;
            screenHeight = _screenHeight;//((InfoMediaDialog)Owner).ScreenHeight;



            //pictureBox1.Width = screenWidth;
            //pictureBox1.Height = screenHeight;

            //pictureBox1.Location = new Point(0, 0);

            localPath = path;
            imageElementList = images;
            webCamReader = camReader;

            calibGridDimension = gridDimension;
            calibPointX = -1;
            calibPointY = -1;

            isClickable = clickable;
            mouseClicked = false;

            isOnlineAnalysis = eyeGaze || emotionDetection || recordVideo;
            recPath = resultsPath + "\\" + outputName + ".avi";

            _recordVideo = 0;
            _recordAudio = 0;
            _eyeGaze = 0;
            _emotionDetection = 0;
            if (recordVideo)
                _recordVideo = 1;
            if (recordAudio)
                _recordAudio = 1;
            if (eyeGaze)
                _eyeGaze = 1;
            if (emotionDetection)
                _emotionDetection = 1;

            errMessage = "";
            warnMessage = "";
            fpsMessage = "";
            imageSetTime = DateTime.MinValue;
           // DialogResult = DialogResult.OK;

            insightCode = insightPass;

            log_errors = PromoTester1.Properties.Settings.Default.debug_mode ? 1 : 0;

            start_time = DateTime.Now;
            image_quality_start = 0;
            image_quality_min = 1000;
            image_quality_sum = 0;
            image_quality_count = 0;
            head_movement_max = 0;
            head_movement_sum = 0;
            head_movement_count = 0;
            head_size_ratio_max = 0;
            head_size_ratio_sum = 0;
            head_size_ratio_count = 0;

        }
        #endregion

    }
}
