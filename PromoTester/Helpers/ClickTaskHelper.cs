﻿using System.IO;

namespace PromoTester1
{
    public static class ClickTaskHelper
    {
        public static void GenerateHtmlElementInfo(string path, string infoElementName)
        {

            if (File.Exists($"{path}clicktaskInfo_{infoElementName}.html"))
            {
                File.Delete($"{path}clicktaskInfo_{infoElementName}.html");
            }
            File.AppendAllText($"{path}clicktaskInfo_{infoElementName}.html", "<!DOCTYPE html>\n<html>\n<head>\n<title>HTML</title>\n <style>\n *{\n padding: 0;\n margin: 0;\n box-sizing: border-box; \n}\n.banner-area{\n width: 100%;\n height: 100vh;\n  background: url('../infoImages/" + $"{infoElementName}" + "');\n background-repeat: no-repeat; \n background-position: center; \n background-size: auto;\n }\n </style>\n</head>\n<body>\n<div class='banner-area'>\n  </div>\n</body>\n</html>");
        }

        internal static void GenerateHtmlElement(string path, string infoElementName)
        {

            if (File.Exists($"{path}clickscreen_{infoElementName}.html"))
            {
                File.Delete($"{path}clickscreen_{infoElementName}.html");
            }

            File.AppendAllText($"{path}clickscreen_{infoElementName}.html", "<!DOCTYPE html>\n<html>\n<head>\n<title>HTML</title>\n <style>\n *{\n padding: 0;\n margin: 0;\n box-sizing: border-box; \n}\n.banner-area{\n width: 100%;\n height: 100vh;\n  background: url('../clickImages/" + $"{infoElementName}" + "');\n background-position: center;\n background-size: contain;\n background-repeat: no-repeat; }\n </style>\n</head>\n<body>\n<div class='banner-area'>\n  </div>\n</body>\n</html>");
        }
    }
}
