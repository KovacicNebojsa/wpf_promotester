﻿using System.IO;

namespace PromoTester1
{
    public static class WebTaskHelper
    {
        public static void GenerateHtmlElementInfo(string path, string infoElementName)
        {

            if (File.Exists($"{path}webtaskInfo_{infoElementName}.html"))
            {
                File.Delete($"{path}webtaskInfo_{infoElementName}.html");
            }
            File.AppendAllText($"{path}webtaskInfo_{infoElementName}.html", "<!DOCTYPE html>\n<html>\n<head>\n<title>HTML</title>\n <style>\n *{\n padding: 0;\n margin: 0;\n box-sizing: border-box; \n}\n.banner-area{\n width: 100%;\n height: 100vh;\n  background: url('../infoImages/" + $"{infoElementName}" + "');\n background-repeat: no-repeat; \n background-position: center; \n background-size: auto;\n }\n </style>\n</head>\n<body>\n<div class='banner-area'>\n  </div>\n</body>\n</html>");
        }
    }
}
