﻿
using System;
using System.Collections.Generic;
using System.Xml;

namespace PromoTester1
{
    public static class Helper
    {
        public static List<XmlElement> ReadAllXmlElements(XmlNode element)
        {
            List<XmlElement> xmlElements = new List<XmlElement>();

            if (element.HasChildNodes)
            {
                for (int i = 0; i < element.ChildNodes.Count; i++)
                {
                    XmlNode node = element.ChildNodes[i];

                    if (node.NodeType == XmlNodeType.Element)
                    {
                        xmlElements.Add(node as XmlElement);
                    }
                }
            }
            return xmlElements;
        }
        public static string TakeOnlyElementName(XmlElement element, string attribute)
        {
            string infoElementName = element.GetAttribute($"{attribute}");
            var result = infoElementName.Split('/');
            infoElementName = result[1];
            return infoElementName;
        }

        public static void GenerateHtmlForAllElements(List<XmlElement> allElementsInDefinition, string path)
        {
            List<XmlElement> elementsToDisplay = null;
            foreach (var element in allElementsInDefinition)
            {
                if ((element.Name.Trim().CompareTo("info") == 0) && !string.IsNullOrWhiteSpace(element.GetAttribute("src")))
                {
                    string infoElementName = TakeOnlyElementName(element, "src");
                    InfoElementsHelper.GenerateHtmlElement(path, infoElementName);
                }
                if (element.Name.Trim().CompareTo("calibration") == 0)
                {
                    if (element.HasAttribute("info"))
                    {
                        string infoElementName = TakeOnlyElementName(element, "info");
                        CalibrationHelper.GenerateHtmlElement(path, infoElementName);
                    }
                    if (element.HasAttribute("output")) //PROVJERI TREBA LI, moze li biti bez outputa i ChildNodes
                    {
                       //DOPISATI LOGIKU
                    }
                }
                if (element.Name.Trim().CompareTo("testcalibration") == 0)
                {
                    if (element.HasAttribute("info"))
                    {
                        string infoElementName = TakeOnlyElementName(element, "info");
                        TestCalibrationHelper.GenerateHtmlElement(path,infoElementName);
                    }
                    if (element.HasAttribute("output")) //PROVJERI TREBA LI, moze li biti bez outputa i ChildNodes
                    {
                        //DOPISATI LOGIKU
                    }
                }
                if (element.Name.Trim().CompareTo("watchtask") == 0)
                {
                    if (element.HasAttribute("info"))
                    {
                        string infoElementName = TakeOnlyElementName(element, "info");
                        WatchTaskHelper.GenerateHtmlElementInfo(path, infoElementName);
                       
                    }
                    if (element.HasAttribute("output")) //PROVJERI TREBA LI, moze li biti bez outputa i ChildNodes
                    {
                        if (element.HasChildNodes)
                        {
                            elementsToDisplay = ReadAllXmlElements(element);
                            if (elementsToDisplay.Count != 0)
                            {
                                for (int i = 0; i < elementsToDisplay.Count; i++)
                                {
                                    string infoElementName = TakeOnlyElementName(elementsToDisplay[i], "src");
                                    WatchTaskHelper.GenerateHtmlElement(path, infoElementName);
                                }
                            }
                        }
                    }
                }
                if (element.Name.Trim().CompareTo("clicktask") == 0)
                {
                    if (element.HasAttribute("info"))
                    {
                        string infoElementName = TakeOnlyElementName(element, "info");
                        ClickTaskHelper.GenerateHtmlElementInfo(path, infoElementName);
                    }
                    if (element.HasAttribute("output"))
                    {
                        if (element.HasChildNodes)
                        {
                            elementsToDisplay = ReadAllXmlElements(element);
                            if (elementsToDisplay.Count != 0)
                            {
                                for (int i = 0; i < elementsToDisplay.Count; i++)
                                {
                                    string infoElementName = TakeOnlyElementName(elementsToDisplay[i], "src");
                                    ClickTaskHelper.GenerateHtmlElement(path, infoElementName);

                                }
                            }
                        }
                    }
                }
                if (element.Name.Trim().CompareTo("webtask") == 0)
                {
                    if (element.HasAttribute("info"))
                    {
                        string infoElementName = TakeOnlyElementName(element, "info");
                        WebTaskHelper.GenerateHtmlElementInfo(path, infoElementName);
                    }
                }
            }
        }


        /// <summary>
        /// Filling up all list of definition tasks.
        /// Excluding infoElements and allelementsIndefinitions, the elements in these methods are separated from the index based on 0 which is preserved for the main elements that contain most important information about element
        /// Child nodes of elements starts from index 1
        /// </summary>
        /// <param name="allElementsInDefinition"> All elements in definition xml file </param>
        /// <param name="infoElemets"> Has no child nodes </param>
        /// <param name="calibrationElements"> List that contains main (info element), containts images with calibration points  </param>
        /// <param name="testCalibrationElements"> List that contains main (info element), containts images with testcalibration points </param>
        /// <param name="watchtaskElements"> List that contains main (info element) and watchscreens(images to display) </param>
        /// <param name="clicktaskElements">List that contains main (info element) and clickscreen child node elements</param>
        /// <param name="webtaskElements">List that contains main (info element) and webscreen child node elements</param>
        internal static void SeperateElements(List<XmlElement> allElementsInDefinition, ref List<XmlElement> infoElemets, ref List<XmlElement> calibrationElements, ref List<XmlElement> testCalibrationElements, ref List<XmlElement> watchtaskElements, ref List<XmlElement> clicktaskElements, ref List<XmlElement> webtaskElements)
        {
            foreach (var element in allElementsInDefinition)
            {
                if ((element.Name.Trim().CompareTo("info") == 0) && !string.IsNullOrWhiteSpace(element.GetAttribute("src")))
                {
                    infoElemets.Add(element);
                }
                if ((element.Name.Trim().CompareTo("calibration") == 0))
                {
                    //adding main task of element which contains important information about element settings
                    calibrationElements.Add(element);

                    //browsing through all elements of one main element and adding it in list starting from index 1
                    if (element.HasChildNodes)
                    {
                        calibrationElements.AddRange(ReadAllXmlElements(element));
                    }
                }
                if ((element.Name.Trim().CompareTo("testcalibration") == 0))
                {
                    testCalibrationElements.Add(element);
                    if (element.HasChildNodes)
                    {
                        testCalibrationElements.AddRange(ReadAllXmlElements(element));
                    }
                }
                if ((element.Name.Trim().CompareTo("watchtask") == 0))
                {
                    watchtaskElements.Add(element);
                    if (element.HasChildNodes)
                    {
                        watchtaskElements.AddRange(ReadAllXmlElements(element));
                    }
                }
                if ((element.Name.Trim().CompareTo("clicktask") == 0))
                {
                    clicktaskElements.Add(element);
                    clicktaskElements.Add(element);
                    if (element.HasChildNodes)
                    {
                        clicktaskElements.AddRange(ReadAllXmlElements(element));
                    }
                }
                if ((element.Name.Trim().CompareTo("webtask") == 0))
                {
                    webtaskElements.Add(element);
                }
            }
        }

        public static double GetUnixTimestamp()
        {
            return (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
        }

        public static double GetUnixTimestamp(DateTime date)
        {
            return (date.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
        }

    }
}

