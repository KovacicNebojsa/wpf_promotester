﻿using PromoTester1.WebCamera;
using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows;

namespace PromoTester1
{
    public static class WebCamHelper
    {
        public static void CheckForCameraAndStart(WebCamReader webCamReader)
        {
            try
            {
                String video_input_list;
                StringBuilder tmp_video_input_list = new StringBuilder(1000);
                OpenCVCamera.FindVideoInputandList(tmp_video_input_list);

                video_input_list = tmp_video_input_list.ToString();
                String[] webcams = video_input_list.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                if (webcams.Length > 0)
                {
                    WebCamSelectDialog wcsd = new WebCamSelectDialog(webcams);
                    bool? result = wcsd.ShowDialog();
                    if (result == true)
                    {
                        PromoTester1.Properties.Settings.Default.webCamId = wcsd.SelectedWebcamId;
                        PromoTester1.Properties.Settings.Default.Save();
                        webCamReader = new WebCamReader(PromoTester1.Properties.Settings.Default.webCamId);
                        Thread webCamCheckThread = new Thread(new ThreadStart(webCamReader.camCheckForever));
                        webCamCheckThread.IsBackground = true;
                        webCamCheckThread.Start();
                        WebcamCheckDialog wb = new WebcamCheckDialog(webCamReader);
                        wb.ShowDialog();

                    }
                }
                else
                {
                    MessageBox.Show(PromoTester1.Properties.Resources.noWebcamError, PromoTester1.Properties.Resources.error_caption, MessageBoxButton.OK, MessageBoxImage.Error);
                    File.AppendAllText(PromoTester1.Properties.Settings.Default.errorsLogFileName, Helper.GetUnixTimestamp() + " " + PromoTester1.Properties.Resources.noWebcamError + Environment.NewLine);
                }
            }
            catch
            {
                MessageBox.Show(PromoTester1.Properties.Resources.noWebcamError, PromoTester1.Properties.Resources.error_caption, MessageBoxButton.OK, MessageBoxImage.Error);
                File.AppendAllText(PromoTester1.Properties.Settings.Default.errorsLogFileName, Helper.GetUnixTimestamp() + " " + PromoTester1.Properties.Resources.noWebcamError + Environment.NewLine);
            }
        }
        public static int GetCodecFourccCode()
        {
            int codec = -1;
            if (PromoTester1.Properties.Settings.Default.rec_video_fourcc_codec.CompareTo("divx") == 0)
            {
                codec = 1482049860;
            }
            else if (PromoTester1.Properties.Settings.Default.rec_video_fourcc_codec.CompareTo("xvid") == 0)
            {
                codec = 1145656920;
            }

            return codec;
        }
    }
}
