﻿using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace PromoTester1
{
    public static class TestCalibrationHelper
    {
        public static void GenerateHtmlElement(string path, string infoElementName)
        {
            if (File.Exists($"{path}testcalibrationInfo_{infoElementName}.html"))
            {
                File.Delete($"{path}testcalibrationInfo_{infoElementName}.html");
            }
            File.AppendAllText($"{path}testcalibrationInfo_{infoElementName}.html", "<!DOCTYPE html>\n<html>\n<head>\n<title>HTML</title>\n <style>\n *{\n padding: 0;\n margin: 0;\n box-sizing: border-box; \n}\n.banner-area{\n width: 100%;\n height: 100vh;\n  background: url('../infoImages/" + $"{infoElementName}" + "');\n background-repeat: no-repeat; \n background-position: center; \n background-size: auto;\n }\n </style>\n</head>\n<body>\n<div class='banner-area'>\n  </div>\n</body>\n</html>");
        }

        public static void GeneratePointsOnCanvas(string path, List<XmlElement> calibrationList)
        {
            int calibGridDimensionX = 0;
            int calibGridDimensionY = 0;
            string infoElementName = "";
            for (int i = 0; i < calibrationList.Count; i++)
            {

                if (i == 0)
                {
                    calibGridDimensionX = int.Parse(calibrationList[i].GetAttribute("gridW"));
                    calibGridDimensionY = int.Parse(calibrationList[i].GetAttribute("gridH"));

                    //skip because first element is info element (skip in that case)
                    continue;
                }
                infoElementName = calibrationList[i].GetAttribute("output");
                int calibPointX = int.Parse(calibrationList[i].GetAttribute("calibPointX"));
                int calibPointY = int.Parse(calibrationList[i].GetAttribute("calibPointY"));


                if (File.Exists($"{path}testcalibrationImage_{infoElementName}.jpg.html"))
                {
                    File.Delete($"{path}testcalibrationImage_{infoElementName}.jpg.html");
                }
                File.AppendAllText($"{path}testcalibrationImage_{infoElementName}.jpg.html", "<!DOCTYPE html>\n\n<head>\n <style type='text/css'>\n canvas {\n width: 100%;\n height: 99vh;\n background-position: center;\n background-size: cover;\n}\n\nbody {\n margin: 0;\n} \n </style> \n </head> \n\n <body> \n <canvas></canvas> \n  <script> \n  var canvas = document.querySelector('canvas'); \n canvas.width = window.innerWidth; \n canvas.height = window.innerHeight; \n function CalculateCalibrationPointX(x, calibGridDimension) {  \n let gridCellW = canvas.width / calibGridDimension; \n let deltaW = gridCellW / 2; \n  return gridCellW * x + deltaW \n } \n let pointX = CalculateCalibrationPointX(" + $"{calibPointX}" + ", " + $"{calibGridDimensionX}" + "); \n function CalculateCalibrationPointY(y, calibGridDimension) { \n let gridCellH = canvas.height / calibGridDimension; \n let deltaH = gridCellH / 2; \n  return gridCellH * y + deltaH; \n } \n let pointY = CalculateCalibrationPointY(" + $"{calibPointY}" + ", " + $"{calibGridDimensionY}" + "); \n var c = canvas.getContext('2d'); \n c.beginPath(); \n c.arc(" + "pointX" + ", " + "pointY" + ", 25, 0, 2 * Math.PI); \n c.fillStyle = 'blue'; \n  c.fill(); \n c.stroke(); \n </script> \n </body> \n\n</html>");
            }
        }
    }
}
