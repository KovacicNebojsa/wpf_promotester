﻿using System.IO;

namespace PromoTester1
{
    public static class InfoElementsHelper
    {
        public static void GenerateHtmlElement(string path, string infoElementName)
        {
            if (File.Exists($"{path}info_{infoElementName}.html"))
            {
                File.Delete($"{path}info_{infoElementName}.html");
            }
            File.AppendAllText($"{path}info_{infoElementName}.html", "<!DOCTYPE html>\n<html>\n<head>\n<title>HTML</title>\n <style>\n *{\n padding: 0;\n margin: 0;\n box-sizing: border-box; \n}\n.banner-area{\n width: 100%;\n height: 100vh;\n  background: url('../infoImages/" + $"{infoElementName}" + "');\n background-repeat: no-repeat; \n background-position: center; \n background-size: auto;\n }\n </style>\n</head>\n<body>\n<div class='banner-area'>\n  </div>\n</body>\n</html>");
        }
    }
}
